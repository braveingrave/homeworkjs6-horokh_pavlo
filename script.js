function createNewUser() {
    let firstPrompt = prompt('Whats your firstname?', '');
    let lastPrompt = prompt('Whats your lastname?', '');
    let birthPrompt = prompt('Enter your birthdate.', '');
    
    const newUser = {
        firstName: firstPrompt,
        lastName: lastPrompt,
        birthDay: birthPrompt,
            getLogin: function () {
            return (this.firstName.substr(0, 1) + this.lastName).toLowerCase()
            },

        getAge: function () {
            let from = birthPrompt.split(".");
            let birthdateTimeStamp = new Date(from[2], from[1] - 1, from[0]);
            let cur = new Date();
            let diff = cur - birthdateTimeStamp;
            let birthDate = Math.floor(diff/31557600000);
            return birthDate;
        },
        
        getPassword: function () {
            return (this.firstName.substr(0, 1).toUpperCase() + this.lastName.toLowerCase() + this.birthDay.substr(6, 10))
        },
        }
        return newUser;
    }
    const user = createNewUser();
  console.log(user.getLogin(), user.getPassword(), user.getAge());